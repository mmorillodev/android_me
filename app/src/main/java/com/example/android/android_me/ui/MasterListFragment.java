package com.example.android.android_me.ui;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.example.android.android_me.R;
import com.example.android.android_me.data.AndroidImageAssets;

public class MasterListFragment extends Fragment {
    OnImageClickListener callback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnImageClickListener){
            callback = (OnImageClickListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_master_list, null);
        GridView gridView = v.findViewById(R.id.images_grid_view);
        gridView.setAdapter(new MasterListAdapter(getActivity(), AndroidImageAssets.getAll()));
        gridView.setOnItemClickListener(new GridView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                callback.onClick(i);
            }
        });
        return v;
    }

    public interface OnImageClickListener{
        void onClick(int position);
    }
}
