package com.example.android.android_me.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.android.android_me.R;
import com.example.android.android_me.data.AndroidImageAssets;

import java.util.ArrayList;
import java.util.List;

public class HeadPartFragment extends Fragment {
    private List<Integer> imgResIDs;
    private int currentImg;

    private final String IMG_ID_LIST = "img_id_list";
    private final String CURRENT_IMAGE = "current_image";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_head_part, null);
        final ImageView imageView = v.findViewById(R.id.imgHead);

        if(savedInstanceState != null){
            currentImg = savedInstanceState.getInt(CURRENT_IMAGE);
            imgResIDs = savedInstanceState.getIntegerArrayList(IMG_ID_LIST);
        }
        if(imgResIDs != null) {
            imageView.setImageResource(imgResIDs.get(currentImg));
        }
        else {
            imageView.setImageResource(AndroidImageAssets.getHeads().get(currentImg));
            return v;
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentImg + 1 < imgResIDs.size())
                    imageView.setImageResource(imgResIDs.get(++currentImg));
                else{
                    currentImg = 0;
                    imageView.setImageResource(imgResIDs.get(currentImg));
                }
            }
        });
        return v;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putIntegerArrayList(IMG_ID_LIST, (ArrayList)imgResIDs);
        outState.putInt(CURRENT_IMAGE, currentImg);
    }

    public void setImgResIDs(List<Integer> imgResIDs) {
        this.imgResIDs = imgResIDs;
    }

    public void setCurrentImg(int currentImg) {
        this.currentImg = currentImg;
    }
}
