package com.example.android.android_me.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;

import com.example.android.android_me.R;
import com.example.android.android_me.data.AndroidImageAssets;

public class MainActivity extends AppCompatActivity implements MasterListFragment.OnImageClickListener{

    private int headIndex;
    private int bodyIndex;
    private int legIndex;
    private boolean twoPane;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(findViewById(R.id.android_me_linear_layout) != null){
            twoPane = true;
            findViewById(R.id.btnNext).setVisibility(Button.GONE);
            GridView gridView = findViewById(R.id.images_grid_view);
            gridView.setNumColumns(2);

            if(savedInstanceState == null) {
                HeadPartFragment headPartFragment = new HeadPartFragment();
                headPartFragment.setImgResIDs(AndroidImageAssets.getHeads());

                BodyPartFragment bodyPartFragment = new BodyPartFragment();
                bodyPartFragment.setImgResIDs(AndroidImageAssets.getBodies());

                LegsPartFragment legsPartFragment = new LegsPartFragment();
                legsPartFragment.setImgResIDs(AndroidImageAssets.getLegs());
                Bundle data = getIntent().getExtras();
                if(data != null){
                    headPartFragment.setCurrentImg(data.getInt("headIndex"));
                    bodyPartFragment.setCurrentImg(data.getInt("bodyIndex"));
                    legsPartFragment.setCurrentImg(data.getInt("legIndex"));
                }

                FragmentManager transaction = getSupportFragmentManager();
                transaction.beginTransaction().add(R.id.headPartFragment, headPartFragment).commit();

                transaction.beginTransaction().add(R.id.bodyPartFragment, bodyPartFragment).commit();

                transaction.beginTransaction().add(R.id.legsPartFragment, legsPartFragment).commit();
            }
        }
        else{
            twoPane = false;
        }
    }

    @Override
    public void onClick(int position) {
        int bodyPart = position/12;

        int index = position - 12*bodyPart;
        if(twoPane){

        }
        else {
            switch (bodyPart) {
                case 0:
                    headIndex = index;
                    break;
                case 1:
                    bodyIndex = index;
                    break;
                case 2:
                    legIndex = index;
                    break;
                default:
                    break;
            }

            Bundle data = new Bundle();
            data.putInt("headIndex", headIndex);
            data.putInt("bodyIndex", bodyIndex);
            data.putInt("legIndex", legIndex);

            final Intent intent = new Intent(this, AndroidMeActivity.class);
            intent.putExtras(data);

            findViewById(R.id.btnNext).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(intent);
                }
            });
        }
    }
}
